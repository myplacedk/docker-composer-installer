# docker-compose installer

Go from fresh Linux install to running Docker Compose application with a single command.

### How to use

1. Create a new server. (Tested with Ubuntu 14.04 x64 on Digital Ocean)

2. Run this command (as root) on the server:

    `curl -fsSL 'https://bitbucket.org/myplacedk/docker-composer-installer/raw/master/setup.sh' > ~/setup.sh && bash ~/setup.sh`
   
    or
   
    `wget 'https://bitbucket.org/myplacedk/docker-composer-installer/raw/master/setup.sh' -O- > ~/setup.sh && bash ~/setup.sh`

3. You will be asked about a Git URL, password and branch for your docker-compose application.

### The script will:

* Update the OS (apt-get update+upgrade)
* Install dependencies
* Install Docker and Docker Compose
* Install an Upstart conf so Docker-Compose runs automatically on boot (starts `/docker/*/docker-compose.yml`)

And unless the Git URL was empty:

* Download your Docker Compose application (to `/docker/REPO_NAME/`)
* Build
* Run docker-compose.yml

You can run the script multiple times to add more than one set of docker-compose files.

## More details

Your Git repository must have the following structure:

* `/docker-compose.yml`
* `/post_clone` If this file exists and is executable, it will be executed after the files are downloaded.
* `/pre_up` If this file exists and is executable, it will be executed before the docker images gets started (Usually after building, but no promises.)

I suggest you add files for your services in this pattern:

* `/service_name/Dockerfile`
* `/service_name/config/`