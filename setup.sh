#!/bin/bash

set -o errexit

function prepare {
  DEBIAN_FRONTEND=noninteractive

  if hash docker 2>/dev/null; then
    echo Docker already installed
  else
    apt-get update
    apt-get upgrade -y
  
    apt-get install -y \
      bash-completion \
      curl \
      git \
      nano \
      python-pip \
      screen \
      wget \
      ;

    curl -fsSL https://get.docker.com/ | sh
  fi
  
  if hash docker-compose 2>/dev/null; then
    echo Docker-compose already installed
  else
    pip install docker-compose
  fi
  
  # http://blog.ianholden.com/using-docker-with-upstart/
  cat <<EOT >/etc/init/docker-compose.conf
description "Docker-compose"
author "$(readlink -e -- $0)"

start on filesystem and started docker
stop on runlevel [!2345]

# Automatically restart process if crashed (limit 3 times in 4 minutes)
respawn limit 3 240

# our process forks(clones) many times (maybe 10) so expect fork, or expect daemon are useless.
# we use the technique explained here http://stackoverflow.com/questions/12200217/can-upstart-expect-respawn-be-used-on-processes-that-fork-more-than-twice

# start the container in the pre-start script
pre-start script
  exec 2>>/tmp/pre-start.log
  set -x
  for dir in /docker/*; do [ -f "\$dir/docker-compose.yml" ] || continue; \$( cd "\$dir"; if [ -x pre_up ]; then ./pre_up || { echo ./pre_up failed; exit 1; } fi; docker-compose up -d ); done
end script

# run a process that stays up while our docker container is up. Upstart will track this PID
script
  exec 2>>/tmp/script.log
  set -x
  echo === script
  while docker ps -q | grep ""; do sleep 10; done
end script

# stop docker container after the stop event has completed
post-stop script
  exec 2>>/tmp/post-stop.log
  set -x
  for dir in /docker/*; do [ -f "\$dir/docker-compose.yml" ] || continue; \$( cd "\$dir"; docker-compose stop ); done
end script
EOT
  service docker-compose start
}

function getParms {

  read -p 'Git URL (Only HTTP supported): ' GIT_URL
  if [ -z "$GIT_URL" ]; then
    echo "No Git URL - not installing a Docker application."
    return
  fi
  
  read -p 'Git password: ' -s GIT_PASSWORD
  echo 
  
  read -p 'Git branch [master]: ' GIT_BRANCH
  if [ -z "$GIT_BRANCH" ]; then
    GIT_BRANCH=master
  fi
}

function setup {
  mkdir -p /docker
  cd /docker

  if [ -z "$GIT_URL" ]; then
    return
  fi
  
  git=$(echo "https://myplacedk@bitbucket.org/myplacedk/docker-htpc.git" | sed 's_.*://\(.*\)@\([^/]*\)/[^/]*/\(.*\).git_\1 \2 \3_');
  GIT_USER=$(echo $git | cut -d ' ' -f 1)
  GIT_HOST=$(echo $git | cut -d ' ' -f 2)
  GIT_REPO=$(echo $git | cut -d ' ' -f 3)
  
  echo "machine $GIT_HOST
  login $GIT_USER
  password $GIT_PASSWORD
  " > ~/.netrc
  chmod 600 ~/.netrc
  
  git clone --depth 1 $GIT_URL --single-branch --branch $GIT_BRANCH $GIT_REPO
  
  cd $GIT_REPO
  
  if [ -x post_clone ]; then
   ./post_clone || { echo "./post_clone failed"; exit 1; }
  else
    echo "./post_clone is not an executable file, skipping.";
  fi

  docker-compose build
  
  if [ -f "docker-compose.yml" ] ; then
    if [ -x pre_up ]; then 
      ./pre_up || { echo ./pre_up failed; exit 1; }
    else
      echo "./pre_up is not an executable file, skipping.";
    fi;
    docker-compose up -d; 
  fi
}

# Run upgrade / docker install in background
prepare > /root/step1.log 2>&1 &
PREPARE_PID=$!

exec > >(tee -i /root/step2.log)
exec 2>&1

# Get arguments interactively
getParms

# Follow the output of prepare until it is finished
tail -n 200 -f --pid $PREPARE_PID /root/step1.log

setup | tee /root/step2.log

echo "Done!"
